using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Tallaks.Cartographs.Engine
{
	[RequireComponent(typeof(Button))]
	public class NewGameActivator : MonoBehaviour
	{
		private const int GAME_SCENE_INDEX = 1;

		private void Start()
		{
			GetComponent<Button>().onClick.AddListener((() =>
				{
					SceneManager.LoadSceneAsync(GAME_SCENE_INDEX, LoadSceneMode.Single);
				}));
		}
	}
}
using UnityEngine;
using UnityEngine.UI;

namespace Tallaks.Cartographs.Engine
{
	[RequireComponent(typeof(Button))]
	public class ExitGame : MonoBehaviour
	{
		// Start is called before the first frame update
		private void Start()
		{
			GetComponent<Button>().onClick.AddListener(() => { Application.Quit(); });
		}
	}
}